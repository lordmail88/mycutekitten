<div align="center">
    <img src="docs/app.png" alt="Logo" width="50%">
</div>

# MyCuteKitten React Native Apps
Splash Screen
<img src="docs/1.png" alt="Logo" width="20%">

Welcome Screen
<img src="docs/2.png" alt="Logo" width="20%">

Kitten List Screen
<img src="docs/3.png" alt="Logo" width="20%">

Kitten Detail Screen
<img src="docs/4.png" alt="Logo" width="20%">

### Debug Apk
[Debug APK](https://drive.google.com/file/d/1fHLKE9n_jj4Egj0GJ3x4p-UmEg3X7rj_/view?usp=sharing)

### Release Apk
[Release APK](https://drive.google.com/file/d/1qa51T5muI-JE9v-zrDVo8pPpH_8yYJhJ/view?usp=sharing)

## Boilerplate

[React Native Boilerplate](https://github.com/thecodingmachine/react-native-boilerplate)

## Technology In Use
* React 17.0.1
* React Native 0.64.0
* Redux 4.0.5
* React Redux 7.2.2
* Nativebase UI 2.15.2
and many more (you can see in the package.json)

## Source Data
* Kitten Name Data From Whiskas [here](https://www.whiskas.ca/Content/files/petNamer_en.json)
* Kitten Image from PlaceKitten [here](https://placekitten.com/)
* Kitten Sound Effect from SoundSnap [here](https://www.soundsnap.com/tags/cat?filters=K190YWdzJTNBJTI3Y2F0JTI3K0FORCtfdGFncyUzQSUyN2FuaW1hbCUyNytBTkQrcHVibGlzaGVkKyUyMSUzRCsw&otherfilter=K190YWdzJTNBJTI3Y2F0JTI3K0FORCtfdGFncyUzQSUyN2FuaW1hbCUyNw==&sorting=2&page=1&maxaudio=-10&minaudio=-100&filteredCategories=&filteredSubCategories=&filteredTags=Y2F0JTJDYW5pbWFs&filteredLibraries=&audioLengthChanged=false&searchTerm=Y2F0)

## Directory layout
* `src/Assets`: assets (image, audio files, ...) used by the application
* `src/Components`: presentational components
* `src/Config`: configuration of the application
* `src/Containers`: container components, i.e. the application's screens
* `src/Navigators`: react navigation navigators
* `src/Services`: application services, e.g. API clients
* `src/Stores`: redux actions, reducers and stores
* `src/Translations`: application strings, you can add languages files and be able to translate your app strings
* `src/Theme`: base styles for the application

## Development
1. Clone this git
2. run `yarn install`
3. Connecting your android device and be sure to listed in `adb devices`
3. run `yarn android`


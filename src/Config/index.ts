export const Config = {
  DARK_MODE: true,
  API_URL: 'https://jsonplaceholder.typicode.com/',
  WHISKAS_KITTY_URL: 'https://www.whiskas.ca/',
  PLACEKITTEN_URL: 'https://placekitten.com/',
}

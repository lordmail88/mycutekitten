import React, {useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
    View,
    ActivityIndicator,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    FlatList
} from 'react-native'
import { Container, Spinner, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Segment, Card, CardItem, Item, Input } from 'native-base';
import { useTranslation } from 'react-i18next'
import {useTheme} from "@/Theme";
import SelectKitty from "@/Store/Kitty/SelectKitty";
import ChangeDisplayLength from "@/Store/Kitty/ChangeDisplayLength";
import {navigate, navigateAndSimpleReset} from "@/Navigators/Root";
import {Player} from "@react-native-community/audio-toolkit";
import {KittyDisplayNumber, KittyImage, KittyListItem} from "@/Components";
import SetKittyPageLoading from "@/Store/Kitty/SetKittyPageLoading";
import InitStartup from "@/Store/Startup/Init";
import SetKittyImageLoading from "@/Store/Kitty/SetKittyImageLoading";
import LoadKitty from "@/Store/Kitty/LoadKitty";

const KittyListContainer = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { Layout, Images } = useTheme()
    const kitty_names = useSelector((state) => state.kitty.kitty_names)
    const displayLength = useSelector((state) => state.kitty.displayLength)
    const isPageLoading = useSelector((state) => state.kitty.isPageLoading)
    const loadingImages = useSelector((state) => state.kitty.loadingImages)
    const shuffleKittens = useSelector((state) => state.kitty.shuffleKittens)


    const selectKitty = (kitty) => {
        dispatch(SelectKitty.action({kitty}));
        navigate('KittyDetail');
    }

    const changeDisplayLength = (displayLength) => {
        setPageLoading(true);
        dispatch(ChangeDisplayLength.action({displayLength}))
        dispatch(LoadKitty.action());
        setPageLoading(false);
    }

    const setPageLoading = (isLoading) => {
        dispatch(SetKittyPageLoading.action(({isLoading})))
    }

    const setLoadingImages = (id, isLoading) => {
        dispatch(SetKittyImageLoading.action({loadingId: id, isImageLoading: isLoading}));
    }



    useEffect(() => {
        setPageLoading(false);
        dispatch(LoadKitty.action());
    }, ['dispatch'])

    return (
        <Container>
            <Header noLeft hasSegment >
                <Body>
                    <Title>My Cute Kitten</Title>
                </Body>
            </Header>

            <KittyDisplayNumber lengthMenuNumbers={[30,50,100]} onPress={(num) => {changeDisplayLength(num)}} />
            <FlatList
                data={shuffleKittens}
                refreshing={isPageLoading}
                renderItem={({ item, index, separators }) => (
                    <KittyListItem
                        item={item}
                        index={index}
                        separators={separators}
                        onPress={() => selectKitty(item)}
                        isLoadingImage={loadingImages[item.id]}
                        onLoadEnd={() => {setLoadingImages(item.id, false)}}
                    />
                )}
            />
        </Container>
    )
}

export default KittyListContainer

import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
    View,
    ActivityIndicator,
    TextInput,
    TouchableOpacity, Image,
} from 'react-native'
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Card, CardItem
} from 'native-base';
import {useTranslation} from "react-i18next";
import {useTheme} from "@/Theme";
import {navigate} from "@/Navigators/Root";
import {KittyImage} from "@/Components";
import {Player} from "@react-native-community/audio-toolkit";
import SetKittyImageLoading from "@/Store/Kitty/SetKittyImageLoading";


const KittyDetailContainer = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { Layout, Images } = useTheme()
    const kitty = useSelector((state) => state.kitty.kitty)
    const loadingImages = useSelector((state) => state.kitty.loadingImages)

    const setLoadingImages = (id, isLoading) => {
        dispatch(SetKittyImageLoading.action({loadingId: id, isImageLoading: isLoading}));
    }

    return (
        <Container>
            <Header>
                <Left>
                    <Button transparent small onPress={() => {navigate('KittyList')}}>
                        <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title>{kitty.Name}</Title>
                </Body>
                <Right>
                    <Button transparent small>
                        {kitty.Gender == "F" ?
                            <Icon name="female" style={{color: 'red'}} /> :
                            <Icon name="male" style={{color: 'blue'}} />
                        }
                    </Button>
                </Right>
            </Header>
            <Content>
                <Card>
                    <CardItem button cardBody bordered onPress={() => {new Player(`meow_${Math.floor(Math.random() * 6) + 1  }.mp3`).play();}}>
                        <KittyImage id={kitty.id} isLoading={loadingImages[kitty.id]} onLoadStart={() => {setLoadingImages(kitty.id, true)}} onLoadEnd={() => {setLoadingImages(kitty.id, false)}}  />
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>ID</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.id}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>Name</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.Name}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>Gender</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.Gender}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>Popular Score</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.IsPopular}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>Famous</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.IsFamous}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Left>
                            <Text>Unusual</Text>
                        </Left>
                        <Body>
                            <Text>{kitty.IsUnusual}</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Body>
                            <Text>About</Text>
                            <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel magna non tellus faucibus faucibus vitae a lorem. Etiam consectetur justo accumsan orci tincidunt, porta placerat ex volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus.</Text>

                        </Body>
                    </CardItem>

                </Card>
            </Content>
        </Container>
    )
}

export default KittyDetailContainer

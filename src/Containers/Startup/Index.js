import React, { useEffect } from 'react'
import { ActivityIndicator, View } from 'react-native'
import { useTheme } from '@/Theme'
import { useDispatch } from 'react-redux'
import InitStartup from '@/Store/Startup/Init'
import { useTranslation } from 'react-i18next'
import { Brand } from '@/Components'
import {
    Player
} from '@react-native-community/audio-toolkit';
import { Container, Header, Content, H1, H2, H3, Text } from 'native-base';

const IndexStartupContainer = () => {
  const { Layout, Gutters, Fonts } = useTheme()

  const { t } = useTranslation()

  const dispatch = useDispatch()


  useEffect(() => {
    dispatch(InitStartup.action());
      new Player('meow.mp3').play();
  }, [dispatch])

  return (
    <View style={[Layout.fill, Layout.colCenter]}>
      <Brand />
      <ActivityIndicator size={'large'} style={[Gutters.largeVMargin]} />
        <H1>{t('welcome')}</H1>
    </View>
  )
}

export default IndexStartupContainer

import React from 'react'
import { View, Image } from 'react-native'
import {Button, Segment, Text} from "native-base";

const KittyDisplayNumber = ({ lengthMenuNumbers = [10,100,1000], onPress }) => {
    return (
        <Segment>
            {lengthMenuNumbers.map((num,index) =>
                <Button key={index} first onPress={() => onPress(num)}>
                    <Text>{num}</Text>
                </Button>
            )}
        </Segment>
    );
}

export default KittyDisplayNumber
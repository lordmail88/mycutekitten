import React from 'react'
import { View, Image } from 'react-native'
import {Body, Button, Card, CardItem, Icon, Right, Segment, Text} from "native-base";
import KittyImage from "./KittyImage";
const KittyListItem = ({ item, index, separators, onPress, isLoadingImage, onLoadStart, onLoadEnd }) => {
    return (
        <Card>
            <CardItem button cardBody bordered onPress={() => onPress()}>
                <KittyImage id={item.id} isLoading={isLoadingImage} onLoadEnd={onLoadEnd} />
            </CardItem>
            <CardItem button  header bordered onPress={() => onPress()}>
                <Body>
                    <Text>{index+1}. {item.Name}</Text>
                </Body>
                <Right>
                    {item.Gender == "F" ?
                        <Icon name="female" style={{color: 'red'}} /> :
                        <Icon name="male" style={{color: 'blue'}} />
                    }

                </Right>
            </CardItem>

        </Card>
    );
};

export default KittyListItem;
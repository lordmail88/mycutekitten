import React from 'react'
import { View, Image } from 'react-native'
import { Spinner } from 'native-base';
const KittyImage = ({id, isLoading = true, onLoadStart, onLoadEnd}) => {
    return (
        <View style={{flex:1}}>
            { isLoading ? <Spinner color='blue' /> : <View/>}
            <Image source={{uri:"https://placekitten.com/"+(id+200)+"/200"}} style={{height: 200, width: null, flex: 1, opacity: isLoading ? 0.1 : 1}} onLoadStart={onLoadStart} onLoadEnd={onLoadEnd} />
        </View>



    );
}

export default KittyImage;
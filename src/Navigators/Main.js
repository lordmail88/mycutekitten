import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { IndexExampleContainer, KittyListContainer, KittyDetailContainer } from '@/Containers'

const Stack = createStackNavigator();

// @refresh reset
const MainNavigator = () => {
  return (
      <Stack.Navigator headerMode={"none"}>
          <Stack.Screen name="KittyList" component={KittyListContainer} />
          <Stack.Screen name="KittyDetail" component={KittyDetailContainer} />
      </Stack.Navigator>
  )
}

export default MainNavigator

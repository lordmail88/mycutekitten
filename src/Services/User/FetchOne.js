import { UserApi, handleError } from '@/Services'

export default async (userId) => {
  if (!userId) {
    return handleError({ message: 'User ID is required' })
  }
  const response = await UserApi.get(`users/${userId}`)
  return response.data
}

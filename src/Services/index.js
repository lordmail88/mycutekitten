import axios from 'axios'
import { Config } from '@/Config'

const UserApi = axios.create({
  baseURL: Config.API_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 3000,
});



const handleError = ({ message, data, status }) => {
  return Promise.reject({ message, data, status })
}

UserApi.interceptors.response.use(
  (response) => response,
  ({ message, response: { data, status } }) => {
    return handleError({ message, data, status })
  },
)

// Whiskas Kitty Name API
const WhiskasKittyApi = axios.create({
  baseURL: Config.WHISKAS_KITTY_URL,
  responseType: 'blob',
  headers: {
    'Content-Type': 'application/json',
  },
  maxContentLength: 100000000,
  maxBodyLength: 1000000000
});

const handleKittyError = ({ message, data, status }) => {
  return Promise.reject({ message, data, status })
}
WhiskasKittyApi.interceptors.response.use(
  (response) => response,
  ({ message, response: { data, status } }) => {
    return handleKittyError({ message, data, status })
  },
)


export {UserApi, WhiskasKittyApi, handleError, handleKittyError}

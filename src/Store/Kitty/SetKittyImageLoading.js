import { createAction } from '@reduxjs/toolkit'

export default {
    initialState: {},
    action: createAction('kitty/setKittyImageLoading'),
    reducers(state, { payload }) {
        if (typeof payload.loadingId !== 'undefined' && typeof payload.isImageLoading !== 'undefined') {
            let arr = [...state.loadingImages];
            arr[payload.loadingId] = payload.isImageLoading;
            state.loadingImages = arr;
        }


    },
}
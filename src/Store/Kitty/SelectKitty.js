import { createAction } from '@reduxjs/toolkit'

export default {
    initialState: {},
    action: createAction('kitty/selectKitty'),
    reducers(state, { payload }) {
        if (typeof payload.kitty !== 'undefined') {
            state.kitty = payload.kitty;
            console.log(payload.kitty);
        }


    },
}
import { createAction } from '@reduxjs/toolkit'

export default {
    initialState: {},
    action: createAction('kitty/loadKitty'),
    reducers(state, { payload }) {
        state.shuffleKittens = state.kitty_names.map((a, idx) => ({rnd: Math.random(), value: a, id: idx+1 }))
            .sort((a, b) => a.rnd - b.rnd)
            .map((a) => a.value)
            .slice(0, state.displayLength);


    },
}
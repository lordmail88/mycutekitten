import {
    buildAsyncState,
    buildAsyncReducers,
    buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper'
import fetchNameKittyService from '@/Services/Kitty/FetchName'

export default {
    initialState: buildAsyncState('fetchName'),
    action: buildAsyncActions('kitty/fetchName', fetchNameKittyService),
    reducers: buildAsyncReducers({
        errorKey: 'fetchName.error', // Optionally, if you scoped variables, you can use a key with dot notation
        loadingKey: 'fetchName.loading',
    }),
}

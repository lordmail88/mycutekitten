import { createAction } from '@reduxjs/toolkit'

export default {
    initialState: {},
    action: createAction('kitty/setKittyPageLoading'),
    reducers(state, { payload }) {
        if (typeof payload.isLoading !== 'undefined') {
            state.isPageLoading = payload.isLoading;
        }


    },
}
import { createAction } from '@reduxjs/toolkit'

export default {
    initialState: {},
    action: createAction('kitty/changeDisplayLength'),
    reducers(state, { payload }) {
        if (typeof payload.displayLength !== 'undefined') {
            state.displayLength = payload.displayLength;

        }


    },
}
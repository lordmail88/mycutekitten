/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function () {
  return {
    logo: require('@/Assets/Images/TOM.png'),
    logo_cute: require('@/Assets/Images/cute_kitten.png'),
  }
}
